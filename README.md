# Datastore Commons

## Functionality
This module defines the Structure for the Datastore.

## Usage
The Commons component is used to define the **data structure** for Kafka and HDFS.

Topics, Keys and the actual Structure is defined in separate classes.
The relation between those three elements is displayed in the picture below.
![Kafka Structure](images/kafka_structure.PNG "Kafka Structure")

Topics can have more than one key. This also means it can hold more than one different structure type. 
This is especially useful when data sources offer different types of data with various structures.
A Key always maps to one distinct structure. Therefore, having the key of data the structure is known as well.
A structure can be used in more than one key.

#### Defining a structure
To define a structure a simple Java class can be used. The place where class is defined doesn't matter, yet as a standard
a folder named *inbound* is used to hold all data structures from collected data sources.
The *openData* folder holds structures from Open Data topics.
Below is an example of a simple data structure used for testing purposes.
 ```java

public class TestApi extends KafkaMessage {
    private Integer id;
    private String firstname;
    private String lastname;

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
```
As you can see the class extends the abstract class `KafkaMessage`, which provides the interface `Serializable` as well as a timestamp. 
The reason behind this overhead is to support [Spark Schemas](http://spark.apache.org/docs/latest/structured-streaming-programming-guide.html#schema-inference-and-partition-of-streaming-dataframesdatasets), which is mandatory to use some operations in Spark.

> Hint: In IntelliJ you can use _Code > Generate.._
> to generate Getter and Setters automatically

In case of nested data in the schema subclasses can be used in the same way as specified above.
Below is an example of nested data in JSON and the resulting Java structure class.
 ```json
{
"coord": {
"lon": 123,
"lat": 456
},
"timestamp": "2020-08-21T12:23:18.681Z"
}
```
The resulting structure needs one additional java classes to specify the nested object.
 ```java
public class Coordinates implements Serializable {
    public Integer lon;
    public Integer lat;

// Getters and setters ...
}
```
The resulting java class for the whole structure can then be implemented as follows.
 ```java
public class NestedExample extends KafkaMessage {
    private Coordinates coords;

// Getters and setters ...
}
```
Arrays in a JSON file can simply be implemented as a list in Java.
 ```json
{
"values": [1, 2, 3, 4, 5],
"timestamp": "2020-08-21T12:23:18.681Z"
}
```
The Json above results in the following strucutre class.
 ```java
public class ArrayExample extends KafkaMessage {
    private List<Integer> values;

// Getters and setters ...
}
```
#### Defining a Key
The [KafkaKeys](../Commons/src/main/java/io/piveau/DataStore/Commons/schema/KafkaKeys.java) class in the schema folders holds all
defined Keys. It all starts with a **KeyFactory** statically defined in the class.
The implementation of the KeyFactory in case of needed adjustment can be found in the *base* folder.
As the KeyFactory is a *Singleton*, you can create it statically by calling the getInstance method on the KeyFactory class itself.
```java
public class KafkaKeys {
    private static KeyFactory factory = KeyFactory.getInstance();
}
```
After this we can use the KeyFactory to staticlly create Kafka Keys in the **KafkaKeys** class.
```java
public class KafkaKeys {
    protected static KeyFactory factory = KeyFactory.getInstance();
    
    public static final Key<TestApi> INBOUND_TESTAPI_KEY =
                factory.getBuilder(TestApi.class)
                .keyName("inbound.TestApi.Key")
                .addSparkOption("timestampFormat", "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZ")
                .orderPartitionByKey(false)
                .build();
}
```
The class Key is typed on the Structure we defined earlier. This guarantees that each key holds exactly one structure.
Additionally, we define a **keyName**. This name will be used as an identifier and should be unique for each Key.
In case of keyName duplicates an error at runtime will be thrown whenever the KafkaKeys class will be used.

The SparkOption is needed for instance to give Spark a hint for the timestampFormat. This is mandatory whenever using a timestamp in the structure.

**OrderPartitionByKey** can be activated in case many keys are used on one kafka topic. It ensures that all data with this key
will be saved in the same kafka partition. Normally this would lead to loss of performance. Data of that Key will then only be handled by one Spark Executor.
In some cases this can reduce the shuffle process of Spark and can also lead to better performance. For default purposes it is better to leave it off.


Additionally the *valueOf* function defined in the KafkaKeys class can be handy when resolving a keyName to its key class.
```java
public class KafkaKeys {
    protected static KeyFactory factory = KeyFactory.getInstance();
    
    public static final Key<TestApi> INBOUND_TESTAPI_KEY =
                factory.getBuilder(TestApi.class)
                .keyName("inbound.TestApi.Key")
                .addSparkOption("timestampFormat", "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZ")
                .orderPartitionByKey(false)
                .build();

public static Key<?> valueOf(String keyName) {
        return factory.keys.get(keyName);
    }

}
```
#### Defining a Topic
The [KafkaTopics](../Commons/src/main/java/io/piveau/DataStore/Commons/schema/KafkaTopics.java) class is used to define Kafka Topics in a similar manner as done with Keys.
Additionally to creating the TopicFactory instance. It is useful to define some Prefixes for Inbound and Opendata as static strings.

 ```java
 public class KafkaTopics {
     public static final String OPENDATA_PREFIX = "OpenData.";
     public static final String INBOUND_PREFIX ="Inbound.";
     private static final TopicFactory topicFactory = TopicFactory.getInstance();
}
```
After this Topics for inbound data and OpenData can be defined, by giving them a **topicName** and registering Keys to it.
 ```java
 public class KafkaTopics {
     public static final String OPENDATA_PREFIX = "OpenData.";
     public static final String INBOUND_PREFIX ="Inbound.";
     private static final TopicFactory topicFactory = TopicFactory.getInstance();
    
    public static final Topic INBOUND_TESTAPI = topicFactory.getBuilder()
                .topicName(INBOUND_PREFIX + "TestApi")
                .addKey(KafkaKeys.INBOUND_TESTAPI_KEY)
                .build();
    
    public static final Topic OPENDATA_TEST = topicFactory.getBuilder()
                .topicName(OPENDATA_PREFIX + "Test")
                .addKey(KafkaKeys.INBOUND_TESTAPI_KEY)
                .build();
}
```

Similar to the KafkaKeys class a *valueOf* is defined here as well. Additionally, functions for resolving all OpenData Topics and Inbound topics are defined as well.
 ```java
 public class KafkaTopics {

    //Previous definitions ....

    
    public static Topic valueOf(String topicName){
            return topicFactory.topics.get(topicName);
        }
    
    public static Set<Topic> openDataTopics(){
        return topicFactory.topics.entrySet().stream()
                .filter(e -> e.getKey().startsWith(OPENDATA_PREFIX))
                .map(Map.Entry::getValue)
                .collect(Collectors.toSet());
    }
    public static Set<Topic> inboundTopics(){
        return topicFactory.topics.entrySet().stream()
                .filter(e -> e.getKey().startsWith(INBOUND_PREFIX))
                .map(Map.Entry::getValue)
                .collect(Collectors.toSet());
    }

}
```


#### DataStore Partitioner
The DatastorePartitioner class is used as a Partitioner for Kafka to implement the functionality the **OrderByPartition**
function has by implementing a Partitioner.
More about that topic can be found here:

https://dzone.com/articles/custom-partitioner-in-kafka-lets-take-quick-tour

To use the partition class the following config option need to be present when using Kafka.

|Key |Value | 
|---| ---|
|partitioner.class  |io.piveau.DataStore.Commons.schema.DataStorePartitioner  |

####HdfsStrucutre
The HdfsStructure class is used to ensure a coherent HDFS structure.
To use it in a project you need to create a new object. Below is an example how to create a new HdfsStructure object.

```java
class BaseApplication {
protected static HdfsStructure hdfsStructure = new HdfsStructure("hdfs://localhost:9000/Piveau/DataStore");
}
```


