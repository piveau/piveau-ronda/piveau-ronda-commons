package io.piveau.DataStore.Commons.schema.harvester.openData.snippets;

import java.io.Serializable;

public class Coordinates implements Serializable {

    private String lat;
    private String lon;

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }
}
