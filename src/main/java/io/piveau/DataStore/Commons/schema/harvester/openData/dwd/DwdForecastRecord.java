package io.piveau.DataStore.Commons.schema.harvester.openData.dwd;

import io.piveau.DataStore.Commons.schema.harvester.KafkaMessage;

import java.sql.Timestamp;
import java.util.List;

public class DwdForecastRecord extends KafkaMessage {

    private List<Forecast> forecast;
    private Timestamp createdAt;
    private String location;

    public List<Forecast> getForecast() {
        return forecast;
    }

    public void setForecast(List<Forecast> forecast) {
        this.forecast = forecast;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
