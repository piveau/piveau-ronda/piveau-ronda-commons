package io.piveau.DataStore.Commons.schema.harvester.inbound.urbanInstitute;

import io.piveau.DataStore.Commons.schema.harvester.inbound.urbanInstitute.snippets.Window;

import java.util.List;

public class DiscovergyEventBatch {

    private Window window;
    private String measurementType;
    private List<DiscovergyEvent> values;
    private Long count;

    public Window getWindow() {
        return window;
    }

    public void setWindow(Window window) {
        this.window = window;
    }

    public List<DiscovergyEvent> getValues() {
        return values;
    }

    public void setValues(List<DiscovergyEvent> values) {
        this.values = values;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public String getMeasurementType() {
        return measurementType;
    }

    public void setMeasurementType(String measurementType) {
        this.measurementType = measurementType;
    }
}
