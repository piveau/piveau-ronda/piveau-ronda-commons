package io.piveau.DataStore.Commons.schema.harvester.openData.smartCity;

import io.piveau.DataStore.Commons.schema.harvester.KafkaMessage;
import io.piveau.DataStore.Commons.schema.harvester.openData.smartCity.snippets.Availability;
import io.piveau.DataStore.Commons.schema.harvester.openData.smartCity.snippets.Energy;
import io.piveau.DataStore.Commons.schema.harvester.openData.snippets.Coordinates;

public class Mobility extends KafkaMessage {

    private String vehicle;
    private String company;
    private Coordinates coordinates;
    private Availability availability;
    private Energy energy;

    public String getVehicle() {
        return vehicle;
    }

    public void setVehicle(String vehicle) {
        this.vehicle = vehicle;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public Coordinates getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(Coordinates coordinates) {
        this.coordinates = coordinates;
    }

    public Availability getAvailability() {
        return availability;
    }

    public void setAvailability(Availability availability) {
        this.availability = availability;
    }

    public Energy getEnergy() {
        return energy;
    }

    public void setEnergy(Energy energy) {
        this.energy = energy;
    }
}
