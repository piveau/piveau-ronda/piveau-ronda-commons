package io.piveau.DataStore.Commons.schema;

import io.piveau.DataStore.Commons.schema.base.Key;
import io.piveau.DataStore.Commons.schema.base.Topic;
import org.apache.kafka.clients.producer.internals.DefaultPartitioner;
import org.apache.kafka.common.Cluster;
import org.apache.kafka.common.record.InvalidRecordException;


/**
 * Kafka Partitioner for the DataStore
 */
public class DataStorePartitioner extends DefaultPartitioner {
    @Override
    public int partition(String topicName, Object keyObject, byte[] bytes, Object o1, byte[] bytes1, Cluster cluster) {
        if (keyObject == null)
            throw new InvalidRecordException("Kafka Key must always be specified for Schema resolution");

        String keyName = new String(bytes);
        Key<?> key = KafkaKeys.valueOf(keyName);
        Topic topic = KafkaTopics.valueOf(topicName);

        if (topic == null)
            throw new InvalidRecordException("Topic:'" + topicName + "' does not exist under KafkaTopics in Commons module");
        if (key == null)
            throw new InvalidRecordException("Try to write to topic:'" + topicName + "', but Key:'" + keyName + "' is does not exist under KafkaKeys in Commons module");

        if (!topic.containsKey(key))
            throw new InvalidRecordException("Kafka Key '" + keyName + " is not registered with topic '" + topicName + "' in Commons module");

        int partition;
        if (key.orderPartitionByKey) {
            partition = super.partition(topicName, keyObject, bytes, o1, bytes1, cluster);
        } else {
            partition = super.partition(topicName, null, null, o1, bytes1, cluster);
        }
        return partition;
    }
}
