package io.piveau.DataStore.Commons.schema.harvester.openData.smartEnergy.gas;

import io.piveau.DataStore.Commons.schema.harvester.KafkaMessage;
import io.piveau.DataStore.Commons.schema.harvester.openData.smartEnergy.snippets.Statistics;

public class Gas extends KafkaMessage {

    private Statistics volume;
    private Statistics actualityDuration;

    public Statistics getVolume() {
        return volume;
    }

    public void setVolume(Statistics volume) {
        this.volume = volume;
    }

    public Statistics getActualityDuration() {
        return actualityDuration;
    }

    public void setActualityDuration(Statistics actualityDuration) {
        this.actualityDuration = actualityDuration;
    }
}
