package io.piveau.DataStore.Commons.schema.harvester.openData.smartEnergy.gas;

import io.piveau.DataStore.Commons.schema.harvester.inbound.urbanInstitute.snippets.Window;

import java.util.List;

public class GasBatch {
    Window window;
    List<Gas> values;

    public Window getWindow() {
        return window;
    }

    public void setWindow(Window window) {
        this.window = window;
    }

    public List<Gas> getValues() {
        return values;
    }

    public void setValues(List<Gas> values) {
        this.values = values;
    }
}