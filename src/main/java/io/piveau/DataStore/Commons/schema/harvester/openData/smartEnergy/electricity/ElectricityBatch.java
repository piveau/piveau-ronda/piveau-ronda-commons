package io.piveau.DataStore.Commons.schema.harvester.openData.smartEnergy.electricity;

import io.piveau.DataStore.Commons.schema.harvester.inbound.urbanInstitute.snippets.Window;

import java.util.List;

public class ElectricityBatch {

    private Window window;
    private List<Electricity> values;

    public Window getWindow() {
        return window;
    }

    public void setWindow(Window window) {
        this.window = window;
    }

    public List<Electricity> getValues() {
        return values;
    }

    public void setValues(List<Electricity> values) {
        this.values = values;
    }
}