package io.piveau.DataStore.Commons.schema.harvester.openData.smartCity.snippets;

import io.piveau.DataStore.Commons.schema.harvester.openData.snippets.TypedValue;

public class Energy {
    public String source;
    public TypedValue<Float> amount;

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public TypedValue<Float> getAmount() {
        return amount;
    }

    public void setAmount(TypedValue<Float> amount) {
        this.amount = amount;
    }
}
