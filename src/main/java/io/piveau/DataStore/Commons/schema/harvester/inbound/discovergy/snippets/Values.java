package io.piveau.DataStore.Commons.schema.harvester.inbound.discovergy.snippets;

import java.io.Serializable;

public class Values implements Serializable {

    private Long energyOut;
    private Long energy2;
    private Long energy1;
    private Long energyOut1;
    private Long power;
    private Long energyOut2;
    private Long energy;
    private Long volume;
    private Long actualityDuration;

    public Long getEnergyOut() {
        return energyOut;
    }

    public void setEnergyOut(Long energyOut) {
        this.energyOut = energyOut;
    }

    public Long getEnergy2() {
        return energy2;
    }

    public void setEnergy2(Long energy2) {
        this.energy2 = energy2;
    }

    public Long getEnergy1() {
        return energy1;
    }

    public void setEnergy1(Long energy1) {
        this.energy1 = energy1;
    }

    public Long getEnergyOut1() {
        return energyOut1;
    }

    public void setEnergyOut1(Long energyOut1) {
        this.energyOut1 = energyOut1;
    }

    public Long getPower() {
        return power;
    }

    public void setPower(Long power) {
        this.power = power;
    }

    public Long getEnergyOut2() {
        return energyOut2;
    }

    public void setEnergyOut2(Long energyOut2) {
        this.energyOut2 = energyOut2;
    }

    public Long getEnergy() {
        return energy;
    }

    public void setEnergy(Long energy) {
        this.energy = energy;
    }

    public Long getVolume() {
        return volume;
    }

    public void setVolume(Long volume) {
        this.volume = volume;
    }

    public Long getActualityDuration() {
        return actualityDuration;
    }

    public void setActualityDuration(Long actualityDuration) {
        this.actualityDuration = actualityDuration;
    }

}
