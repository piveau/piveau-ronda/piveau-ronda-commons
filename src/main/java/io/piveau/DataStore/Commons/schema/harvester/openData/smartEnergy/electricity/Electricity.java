package io.piveau.DataStore.Commons.schema.harvester.openData.smartEnergy.electricity;

import io.piveau.DataStore.Commons.schema.harvester.KafkaMessage;
import io.piveau.DataStore.Commons.schema.harvester.openData.smartEnergy.snippets.Statistics;

public class Electricity extends KafkaMessage {

    private Statistics energy;
    private Statistics energy1;
    private Statistics energy2;
    private Statistics energyOut;
    private Statistics energyOut1;
    private Statistics energyOut2;
    private Statistics power;

    public Statistics getEnergy() {
        return energy;
    }

    public void setEnergy(Statistics energy) {
        this.energy = energy;
    }

    public Statistics getEnergy1() {
        return energy1;
    }

    public void setEnergy1(Statistics energy1) {
        this.energy1 = energy1;
    }

    public Statistics getEnergy2() {
        return energy2;
    }

    public void setEnergy2(Statistics energy2) {
        this.energy2 = energy2;
    }

    public Statistics getEnergyOut() {
        return energyOut;
    }

    public void setEnergyOut(Statistics energyOut) {
        this.energyOut = energyOut;
    }

    public Statistics getEnergyOut1() {
        return energyOut1;
    }

    public void setEnergyOut1(Statistics energyOut1) {
        this.energyOut1 = energyOut1;
    }

    public Statistics getEnergyOut2() {
        return energyOut2;
    }

    public void setEnergyOut2(Statistics energyOut2) {
        this.energyOut2 = energyOut2;
    }

    public Statistics getPower() {
        return power;
    }

    public void setPower(Statistics power) {
        this.power = power;
    }
}
