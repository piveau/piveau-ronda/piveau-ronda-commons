package io.piveau.DataStore.Commons.schema;

import io.piveau.DataStore.Commons.schema.base.Topic;
import io.piveau.DataStore.Commons.schema.base.Topic.TopicFactory;

import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * The valid Topics specified for Kafka
 */
public class KafkaTopics {
    public static final String OPENDATA_PREFIX = "OpenData.";
    public static final String INBOUND_PREFIX = "Inbound.";
    private static final TopicFactory topicFactory = TopicFactory.getInstance();
    public static final Topic INBOUND_OPENWEATHERMAP = topicFactory.getBuilder()
            .topicName(INBOUND_PREFIX + "OpenWeatherMap")
            .addKey(KafkaKeys.INBOUND_OPENWEATHERMAP)
            .build();


    public static final Topic INBOUND_URBANINSTITUTE = topicFactory.getBuilder()
            .topicName(INBOUND_PREFIX + "UrbanInstitute")
            .addKey(KafkaKeys.INBOUND_URBANINSTITUTE_WIFIACCESS)
            .addKey(KafkaKeys.INBOUND_URBANINSTITUTE_WAVESCAPE)
            .addKey(KafkaKeys.INBOUND_URBANINSTITUTE_PULSEGENERIC)
            .addKey(KafkaKeys.INBOUND_URBANINSTITUTE_NODERED)
            .addKey(KafkaKeys.INBOUND_URBANINSTITUTE_EVENTSTREAM)
            .addKey(KafkaKeys.INBOUND_URBANINSTITUTE_DISCOVERGY)
            .addKey(KafkaKeys.INBOUND_URBANINSTITUTE_OPENWEATHER)
            .build();

    public static final Topic INBOUND_URBANINSTITUTE_BATCH_SIMPLE = topicFactory.getBuilder()
            .topicName(INBOUND_PREFIX + "UrbanInstitute.Batch.Simple")
            .addKey(KafkaKeys.OPENDATA_SMARTENERGY_ELECTRICITY)
            .addKey(KafkaKeys.OPENDATA_SMARTENERGY_GAS)
            .addKey(KafkaKeys.OPENDATA_SMARTENERGY_WATER)
            .build();

    public static final Topic INBOUND_URBANINSTITUTE_BATCH_INTERMEDIATE = topicFactory.getBuilder()
            .topicName(INBOUND_PREFIX + "UrbanInstitute.Batch.Intermediate")
            .addKey(KafkaKeys.INBOUND_URBANINSTITUTE_DISCOVERGY_ELEC_BATCH)
            .addKey(KafkaKeys.INBOUND_URBANINSTITUTE_DISCOVERGY_GAS_BATCH)
            .addKey(KafkaKeys.INBOUND_URBANINSTITUTE_DISCOVERGY_WATER_BATCH)
            .build();

    public static final Topic INBOUND_TESTAPI = topicFactory.getBuilder()
            .topicName(INBOUND_PREFIX + "TestApi")
            .addKey(KafkaKeys.INBOUND_TESTAPI_KEY)
            .build();

    public static final Topic INBOUND_DISCOVERGY = topicFactory.getBuilder()
            .topicName(INBOUND_PREFIX + "Discovergy")
            .addKey(KafkaKeys.INBOUND_DISCOVERGY_METER_ALL)
            .build();

    public static final Topic INBOUND_DISCOVERGY_BATCH = topicFactory.getBuilder()
            .topicName(INBOUND_PREFIX + "Discovergy.Batch")
            .addKey(KafkaKeys.INBOUND_DISCOVERGY_BATCH)
            .build();

    public static final Topic INBOUND_DWD = topicFactory.getBuilder()
            .topicName(INBOUND_PREFIX + "DWD")
            .addKey(KafkaKeys.INBOUND_DWD)
            .build();

    public static final Topic OPENDATA_OPENWEATHER_CITY = topicFactory.getBuilder()
            .topicName(OPENDATA_PREFIX + "OpenWeather.City")
            .addKey(KafkaKeys.OPENDATA_CITYWEATHER)
            .build();

    public static final Topic OPENDATA_OPENWEATHER = topicFactory.getBuilder()
            .topicName(OPENDATA_PREFIX + "OpenWeather")
            .addKey(KafkaKeys.INBOUND_URBANINSTITUTE_OPENWEATHER)
            .build();

    public static final Topic OPENDATA_DWD_FORECAST_FRANKFURT = topicFactory.getBuilder()
            .topicName(OPENDATA_PREFIX + "DWD.Forecast.Frankfurt")
            .addKey(KafkaKeys.OPENDATA_DWD_FORECAST)
            .build();

    public static final Topic OPENDATA_DWD_FORECAST_MAINZ = topicFactory.getBuilder()
            .topicName(OPENDATA_PREFIX + "DWD.Forecast.Mainz")
            .addKey(KafkaKeys.OPENDATA_DWD_FORECAST)
            .build();

    public static final Topic OPENDATA_DWD_FORECAST_DARMSTADT = topicFactory.getBuilder()
            .topicName(OPENDATA_PREFIX + "DWD.Forecast.Darmstadt")
            .addKey(KafkaKeys.OPENDATA_DWD_FORECAST)
            .build();

    public static final Topic OPENDATA_DWD_FORECAST_FLUGHAFEN_FRANKFURT = topicFactory.getBuilder()
            .topicName(OPENDATA_PREFIX + "DWD.Forecast.Flughafen_Frankfurt")
            .addKey(KafkaKeys.OPENDATA_DWD_FORECAST)
            .build();

    public static final Topic OPENDATA_DWD_FORECAST_WIESBADEN = topicFactory.getBuilder()
            .topicName(OPENDATA_PREFIX + "DWD.Forecast.Wiesbaden")
            .addKey(KafkaKeys.OPENDATA_DWD_FORECAST)
            .build();

    public static final Topic OPENDATA_SMARTCITY_MOBILITY = topicFactory.getBuilder()
            .topicName(OPENDATA_PREFIX + "SmartCity.Mobility")
            .addKey(KafkaKeys.OPENDATA_SMARTCITY_MOBILITY)
            .build();

    public static final Topic OPENDATA_SMARTCITY_ENVIRONMENT = topicFactory.getBuilder()
            .topicName(OPENDATA_PREFIX + "SmartCity.Environment")
            .addKey(KafkaKeys.OPENDATA_SMARTCITY_ENVIRONMENT)
            .build();

    public static final Topic OPENDATA_SMARTENERGY_ELECTRICITY = topicFactory.getBuilder()
            .topicName(OPENDATA_PREFIX + "SmartEnergy.Electricity")
            .addKey(KafkaKeys.OPENDATA_SMARTENERGY_ELECTRICITY)
            .build();

    public static final Topic OPENDATA_SMARTENERGY_GAS = topicFactory.getBuilder()
            .topicName(OPENDATA_PREFIX + "SmartEnergy.Gas")
            .addKey(KafkaKeys.OPENDATA_SMARTENERGY_GAS)
            .build();

    public static final Topic OPENDATA_SMARTENERGY_Water = topicFactory.getBuilder()
            .topicName(OPENDATA_PREFIX + "SmartEnergy.Water")
            .addKey(KafkaKeys.OPENDATA_SMARTENERGY_WATER)
            .build();

    public static final Topic OPENDATA_TEST = topicFactory.getBuilder()
            .topicName(OPENDATA_PREFIX + "Test")
            .addKey(KafkaKeys.INBOUND_TESTAPI_KEY)
            .build();

    public static final Topic OPENDATA_RMV_DEPARTURE = topicFactory.getBuilder()
            .topicName(OPENDATA_PREFIX + "RMV.Departure")
            .addKey(KafkaKeys.OPENDATA_RMV_DEPARTURE)
            .build();


    public static Topic valueOf(String topicName) {
        return topicFactory.topics.get(topicName);
    }

    public static Set<Topic> openDataTopics() {
        return topicFactory.topics.entrySet().stream()
                .filter(e -> e.getKey().startsWith(OPENDATA_PREFIX))
                .map(Map.Entry::getValue)
                .collect(Collectors.toSet());
    }

    public static Set<Topic> inboundTopics() {
        return topicFactory.topics.entrySet().stream()
                .filter(e -> e.getKey().startsWith(INBOUND_PREFIX))
                .map(Map.Entry::getValue)
                .collect(Collectors.toSet());
    }

}
