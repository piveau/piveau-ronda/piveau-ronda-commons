package io.piveau.DataStore.Commons.schema.base;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * The Topic class is an abstraction of a Kafka Topic.
 * It can hold several Keys and one unique topicName.
 */
public class Topic {
    public final String topicName;
    public final Set<Key<?>> keys;

    private Topic(String topicName, Set<Key<?>> keys) {
        this.topicName = topicName;
        this.keys = keys;
    }

    public boolean containsKey(Key<?> key) {
        return this.keys.contains(key);
    }

    public static class TopicFactory {

        private static TopicFactory instance = null;
        public Map<String, Topic> topics = new HashMap<>();

        private TopicFactory() {
        }

        public static TopicFactory getInstance() {
            if (instance == null) {
                instance = new TopicFactory();
            }
            return instance;
        }

        public TopicBuilder getBuilder() {
            return new TopicBuilder(this);
        }

        private Topic create(TopicBuilder builder) {
            if (builder.topicName == null || builder.topicName.length() < 1) {
                throw new IllegalArgumentException("Topic must have at least one character.");
            }
            if (topics.keySet().stream().anyMatch(topicName -> topicName.equals(builder.topicName))) {
                throw new IllegalArgumentException("Topic Name '" + builder.topicName + "' already exists.");
            }

            Topic topic = new Topic(builder.topicName, builder.keys);

            topics.put(topic.topicName, topic);

            return topic;
        }

    }

    public static class TopicBuilder {
        public String topicName;
        private final TopicFactory factory;
        public Set<Key<?>> keys = new HashSet<>();

        private TopicBuilder(TopicFactory factory) {
            this.factory = factory;
        }

        public Topic build() {
            return factory.create(this);
        }

        public TopicBuilder topicName(String topicName) {
            this.topicName = topicName;
            return this;
        }

        public TopicBuilder keys(Set<Key<?>> keys) {
            this.keys = keys;
            return this;
        }

        public TopicBuilder addKey(Key<?> key) {
            this.keys.add(key);
            return this;
        }

        public TopicBuilder removeKey(Key<?> key) {
            this.keys.remove(key);
            return this;
        }
    }

}
