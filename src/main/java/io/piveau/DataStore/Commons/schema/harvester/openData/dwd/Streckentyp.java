package io.piveau.DataStore.Commons.schema.harvester.openData.dwd;

public class Streckentyp {

    private String Belagstemp;
    private String Zustand;

    public String getBelagstemp() {
        return Belagstemp;
    }

    public void setBelagstemp(String belagstemp) {
        Belagstemp = belagstemp;
    }

    public String getZustand() {
        return Zustand;
    }

    public void setZustand(String zustand) {
        Zustand = zustand;
    }
}
