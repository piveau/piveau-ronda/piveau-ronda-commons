package io.piveau.DataStore.Commons.schema.harvester.inbound.discovergy;

import io.piveau.DataStore.Commons.schema.harvester.KafkaMessage;
import io.piveau.DataStore.Commons.schema.harvester.inbound.discovergy.snippets.Location;
import io.piveau.DataStore.Commons.schema.harvester.inbound.discovergy.snippets.Values;

public class DiscovergyReading extends KafkaMessage {

    private Values values;
    private Location location;

    public Values getValues() {
        return values;
    }

    public void setValues(Values values) {
        this.values = values;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}
