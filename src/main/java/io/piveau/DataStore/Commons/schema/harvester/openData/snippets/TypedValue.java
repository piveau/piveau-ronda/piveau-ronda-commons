package io.piveau.DataStore.Commons.schema.harvester.openData.snippets;

public class TypedValue<T> {

    private T value;
    private String unit;

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
}
