package io.piveau.DataStore.Commons.schema.harvester.inbound.discovergy;

import io.piveau.DataStore.Commons.schema.harvester.inbound.discovergy.snippets.Window;

import java.util.List;

public class DiscovergyReadingBatch {

    private Window window;
    private List<DiscovergyReading> values;
    private Long count;

    public Window getWindow() {
        return window;
    }

    public void setWindow(Window window) {
        this.window = window;
    }

    public List<DiscovergyReading> getValues() {
        return values;
    }

    public void setValues(List<DiscovergyReading> values) {
        this.values = values;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }
}
