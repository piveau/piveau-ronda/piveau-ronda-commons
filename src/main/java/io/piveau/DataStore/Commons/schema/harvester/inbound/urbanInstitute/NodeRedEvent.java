package io.piveau.DataStore.Commons.schema.harvester.inbound.urbanInstitute;

import io.piveau.DataStore.Commons.schema.harvester.KafkaMessage;

public class NodeRedEvent extends KafkaMessage {

    public Float cntNodes;
    public Float cntFlows;

    public Float getCntNodes() {
        return cntNodes;
    }

    public void setCntNodes(Float cntNodes) {
        this.cntNodes = cntNodes;
    }

    public Float getCntFlows() {
        return cntFlows;
    }

    public void setCntFlows(Float cntFlows) {
        this.cntFlows = cntFlows;
    }
}
