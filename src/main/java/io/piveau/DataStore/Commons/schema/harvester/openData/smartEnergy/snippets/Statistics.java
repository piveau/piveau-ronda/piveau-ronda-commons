package io.piveau.DataStore.Commons.schema.harvester.openData.smartEnergy.snippets;

import java.io.Serializable;

public class Statistics implements Serializable {

    private Long min;
    private Long max;
    private Float avg;
    private Float stddev;

    public Long getMin() {
        return min;
    }

    public void setMin(Long min) {
        this.min = min;
    }

    public Long getMax() {
        return max;
    }

    public void setMax(Long max) {
        this.max = max;
    }

    public Float getAvg() {
        return avg;
    }

    public void setAvg(Float avg) {
        this.avg = avg;
    }

    public Float getStddev() {
        return stddev;
    }

    public void setStddev(Float stddev) {
        this.stddev = stddev;
    }
}
