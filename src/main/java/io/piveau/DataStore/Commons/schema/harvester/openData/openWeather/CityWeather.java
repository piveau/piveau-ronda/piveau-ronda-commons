package io.piveau.DataStore.Commons.schema.harvester.openData.openWeather;

import io.piveau.DataStore.Commons.schema.harvester.KafkaMessage;
import io.piveau.DataStore.Commons.schema.harvester.openData.snippets.Coordinates;
import io.piveau.DataStore.Commons.schema.harvester.openData.snippets.TypedValue;

public class CityWeather extends KafkaMessage {

    private Coordinates coordinates;
    private TypedValue<Float> temperature;
    private TypedValue<Float> humidity;
    private TypedValue<Float> pressure;

    public Coordinates getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(Coordinates coordinates) {
        this.coordinates = coordinates;
    }

    public TypedValue<Float> getTemperature() {
        return temperature;
    }

    public void setTemperature(TypedValue<Float> temperature) {
        this.temperature = temperature;
    }

    public TypedValue<Float> getHumidity() {
        return humidity;
    }

    public void setHumidity(TypedValue<Float> humidity) {
        this.humidity = humidity;
    }

    public TypedValue<Float> getPressure() {
        return pressure;
    }

    public void setPressure(TypedValue<Float> pressure) {
        this.pressure = pressure;
    }
}
