package io.piveau.DataStore.Commons.schema.base;

import java.util.HashMap;
import java.util.Map;

/**
 * The Key class is an abstraction of Kafka Keys.
 * It can hold a structure for the definied structure associated with the Key
 * a unqiue keyName
 * sparkOptions for helping Spark parsing the structure
 * and optional ordered Partition on Kafka
 *
 * @param <T> The structure Class for the Key
 */
public class Key<T> {
    public final String keyName;
    public final Class<T> schema;
    public final Map<String, String> sparkOptions;
    public final boolean orderPartitionByKey;

    private Key(String keyName, Map<String, String> options, Class<T> schema, boolean orderPartitionByKey) {
        this.schema = schema;
        this.keyName = keyName;
        this.sparkOptions = options;
        this.orderPartitionByKey = orderPartitionByKey;
    }


    public static class KeyBuilder<T> {
        private final KeyFactory factory;
        public String keyName;
        public Class<T> schema;
        public boolean orderByKey = false;
        public Map<String, String> sparkOptions = new HashMap<>();

        private KeyBuilder(KeyFactory factory) {
            this.factory = factory;
        }

        public Key<T> build() {
            return factory.create(this);
        }

        public KeyBuilder<T> keyName(String keyName) {
            this.keyName = keyName;
            return this;
        }

        public KeyBuilder<T> schema(Class<T> schema) {
            this.schema = schema;
            return this;
        }

        public KeyBuilder<T> sparkOptions(Map<String, String> options) {
            sparkOptions.clear();
            sparkOptions.putAll(options);
            return this;
        }

        public KeyBuilder<T> removeSparkOption(String key) {
            sparkOptions.remove(key);
            return this;
        }

        public KeyBuilder<T> addSparkOption(String key, String value) {
            sparkOptions.put(key, value);
            return this;
        }

        public KeyBuilder<T> orderPartitionByKey(boolean orderByKey) {
            this.orderByKey = orderByKey;
            return this;
        }
    }

    public static class KeyFactory {

        private static KeyFactory instance = null;
        public Map<String, Key<?>> keys = new HashMap<>();

        private KeyFactory() {
        }

        public static KeyFactory getInstance() {
            if (instance == null) {
                instance = new KeyFactory();
            }
            return instance;
        }

        public <T> KeyBuilder<T> getBuilder(Class<T> t) {
            return new KeyBuilder<T>(this).schema(t);
        }

        private <T> Key<T> create(KeyBuilder<T> builder) {
            if (builder.keyName == null || builder.keyName.length() < 1) {
                throw new IllegalArgumentException("keyName must be at least one character");
            }
            if (keys.keySet().stream().anyMatch(keyName -> keyName.equals(builder.keyName))) {
                throw new IllegalArgumentException("keyName '" + builder.keyName + "' is already used");
            }

            Key<T> key = new Key<>(builder.keyName, builder.sparkOptions, builder.schema, builder.orderByKey);
            keys.put(key.keyName, key);
            return key;
        }

    }

}