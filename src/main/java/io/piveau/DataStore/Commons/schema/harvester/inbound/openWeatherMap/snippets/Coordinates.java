package io.piveau.DataStore.Commons.schema.harvester.inbound.openWeatherMap.snippets;

import java.io.Serializable;

public class Coordinates implements Serializable {

    private Float lat;
    private Float lon;

    public Float getLat() {
        return lat;
    }

    public void setLat(Float lat) {
        this.lat = lat;
    }

    public Float getLon() {
        return lon;
    }

    public void setLon(Float lon) {
        this.lon = lon;
    }
}
