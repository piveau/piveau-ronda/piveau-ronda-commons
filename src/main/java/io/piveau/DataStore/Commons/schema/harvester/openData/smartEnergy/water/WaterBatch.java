package io.piveau.DataStore.Commons.schema.harvester.openData.smartEnergy.water;

import io.piveau.DataStore.Commons.schema.harvester.inbound.urbanInstitute.snippets.Window;

import java.util.List;

public class WaterBatch {

    private Window window;
    private List<Water> values;

    public Window getWindow() {
        return window;
    }

    public void setWindow(Window window) {
        this.window = window;
    }

    public List<Water> getValues() {
        return values;
    }

    public void setValues(List<Water> values) {
        this.values = values;
    }
}