package io.piveau.DataStore.Commons.schema;

import io.piveau.DataStore.Commons.schema.base.Key;
import io.piveau.DataStore.Commons.schema.base.Topic;

import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.StringJoiner;

/**
 * The structure of HDFS folders based on a given root directory
 */
public class HdfsStructure {

    public final String rootDir;
    public final String opendataDir;
    public final String checkpointDir;
    public final String uploadDir;

    public HdfsStructure(String rootDir) {
        this.rootDir = rootDir;
        this.opendataDir = new StringJoiner("/")
                .add(rootDir)
                .add("OpenData")
                .toString();
        this.checkpointDir = new StringJoiner("/")
                .add(rootDir)
                .add("checkpoints")
                .toString();
        this.uploadDir = new StringJoiner("/")
                .add(rootDir)
                .add("Upload")
                .toString();
    }

    public String opendataDirectory() {
        return this.opendataDir;
    }

    public String uploadDirectory() {
        return this.uploadDir;
    }

    public String checkpointDirectory() {
        return this.checkpointDir;
    }

    public String checkpointDirectory(String appName, String queryName) {
        return new StringJoiner("/")
                .add(checkpointDir)
                .add(appName.replace("/", "-").replace(":", "_"))
                .add(queryName.replace("/", "-").replace(":", "_"))
                .toString();
    }

    public String topicDirectory(Topic topic, Key<?> key, DataFormat format) {
        if (!topic.containsKey(key)) {
            throw new IllegalArgumentException(key.keyName + " is not present in topic: " + topic.topicName);
        }

        return new StringJoiner("/")
                .add(opendataDir)
                .add(topic.topicName)
                .add(key.keyName)
                .add(format.fileExtension)
                .toString();
    }

    public String topicDirectory(Topic topic, Key<?> key, int year, int month, DataFormat format) {
        return new StringJoiner("/")
                .add(topicDirectory(topic, key, format))
                .add("year=" + year)
                .add("month=" + month)
                .toString();
    }

    public String topicDirectory(Topic topic, Key<?> key, int year, int month, int day, DataFormat format) {
        return new StringJoiner("/")
                .add(topicDirectory(topic, key, year, month, format))
                .add("day=" + day)
                .toString();
    }


    public String topicFileName(Topic topic, int year, int month, int day, DataFormat format) {
        LocalDate currentDate = LocalDate.now();
        LocalDate requestedDate = LocalDate.of(year, month, day);
        String fileCompleteness = requestedDate.isBefore(currentDate) ? "" : "(incomplete)";
        return String.join("-",
                topic.topicName.replace(".", "-"),
                String.valueOf(year),
                String.valueOf(month),
                String.valueOf(day)
        )
                .concat(fileCompleteness)
                .concat("." + format.fileExtension);
    }

    public String topicFileName(Topic topic, int year, int month, DataFormat format) {
        LocalDate currentDate = LocalDate.now();
        LocalDate requestedDate = LocalDate.of(year, month, 1).with(TemporalAdjusters.lastDayOfMonth());
        String fileCompleteness = requestedDate.isBefore(currentDate) ? "" : "(incomplete)";
        return String.join("-",
                topic.topicName.replace(".", "-"),
                String.valueOf(year),
                String.valueOf(month)
        )
                .concat(fileCompleteness)
                .concat("." + format.fileExtension);
    }

}
