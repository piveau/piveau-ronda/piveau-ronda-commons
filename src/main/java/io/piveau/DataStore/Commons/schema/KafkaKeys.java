package io.piveau.DataStore.Commons.schema;


import io.piveau.DataStore.Commons.schema.base.Key;
import io.piveau.DataStore.Commons.schema.base.Key.KeyFactory;
import io.piveau.DataStore.Commons.schema.harvester.inbound.TestApi.TestApi;
import io.piveau.DataStore.Commons.schema.harvester.inbound.discovergy.DiscovergyReading;
import io.piveau.DataStore.Commons.schema.harvester.inbound.discovergy.DiscovergyReadingBatch;
import io.piveau.DataStore.Commons.schema.harvester.inbound.dwd.DwdRecord;
import io.piveau.DataStore.Commons.schema.harvester.inbound.openWeatherMap.OpenWeatherMap;
import io.piveau.DataStore.Commons.schema.harvester.inbound.urbanInstitute.*;
import io.piveau.DataStore.Commons.schema.harvester.openData.dwd.DwdForecastRecord;
import io.piveau.DataStore.Commons.schema.harvester.openData.openWeather.CityWeather;
import io.piveau.DataStore.Commons.schema.harvester.openData.rmv.Departure;
import io.piveau.DataStore.Commons.schema.harvester.openData.smartCity.Environment;
import io.piveau.DataStore.Commons.schema.harvester.openData.smartCity.Mobility;
import io.piveau.DataStore.Commons.schema.harvester.openData.smartEnergy.electricity.Electricity;
import io.piveau.DataStore.Commons.schema.harvester.openData.smartEnergy.electricity.ElectricityBatch;
import io.piveau.DataStore.Commons.schema.harvester.openData.smartEnergy.gas.Gas;
import io.piveau.DataStore.Commons.schema.harvester.openData.smartEnergy.gas.GasBatch;
import io.piveau.DataStore.Commons.schema.harvester.openData.smartEnergy.water.Water;
import io.piveau.DataStore.Commons.schema.harvester.openData.smartEnergy.water.WaterBatch;

/**
 * The valid Keys specified for Kafka
 */
public class KafkaKeys {
    private static KeyFactory factory = KeyFactory.getInstance();
    // OpenWeatherMap
    public static final Key<OpenWeatherMap> INBOUND_OPENWEATHERMAP =
            factory.getBuilder(OpenWeatherMap.class)
                    .keyName("inbound.OpenWeatherMap")
                    .addSparkOption("timestampFormat", "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZ")
                    .build();
    // UrbanInstitute
    public static final Key<EventStreamVSResultEvent> INBOUND_URBANINSTITUTE_EVENTSTREAM =
            factory.getBuilder(EventStreamVSResultEvent.class)
                    .keyName("inbound.UrbanInstitute.205b7dca-cee9-454c-9c86-4641f3616124")
                    .addSparkOption("timestampFormat", "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZ")
                    .build();
    public static final Key<NodeRedEvent> INBOUND_URBANINSTITUTE_NODERED =
            factory.getBuilder(NodeRedEvent.class)
                    .keyName("inbound.UrbanInstitute.205b7dca-cee9-454c-9c86-4641f3616124c")
                    .addSparkOption("timestampFormat", "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZ")
                    .build();
    public static final Key<PulseGenericVSResultEvent> INBOUND_URBANINSTITUTE_PULSEGENERIC =
            factory.getBuilder(PulseGenericVSResultEvent.class)
                    .keyName("inbound.UrbanInstitute.49ba7357-4252-476e-8670-0034b921c2a0")
                    .addSparkOption("timestampFormat", "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZ")
                    .build();
    public static final Key<WaveScapeEvent> INBOUND_URBANINSTITUTE_WAVESCAPE =
            factory.getBuilder(WaveScapeEvent.class)
                    .keyName("inbound.UrbanInstitute.d0795231-3719-4aed-9286-71238508ea40")
                    .addSparkOption("timestampFormat", "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZ")
                    .build();

    public static final Key<WifiAccessPointInsertEvent> INBOUND_URBANINSTITUTE_WIFIACCESS =
            factory.getBuilder(WifiAccessPointInsertEvent.class)
                    .keyName("inbound.UrbanInstitute.null")
                    .addSparkOption("timestampFormat", "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZ")
                    .build();

    public static final Key<DiscovergyEvent> INBOUND_URBANINSTITUTE_DISCOVERGY =
            factory.getBuilder(DiscovergyEvent.class)
                    .keyName("inbound.UrbanInstitute.Discovergy")
                    .addSparkOption("timestampFormat", "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZ")
                    .build();

    public static final Key<DiscovergyEventBatch> INBOUND_URBANINSTITUTE_DISCOVERGY_BATCH =
            factory.getBuilder(DiscovergyEventBatch.class)
                    .keyName("inbound.UrbanInstitute.Discovergy.Batch")
                    .addSparkOption("timestampFormat", "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZ")
                    .build();

    public static final Key<ElectricityBatch> INBOUND_URBANINSTITUTE_DISCOVERGY_ELEC_BATCH =
            factory.getBuilder(ElectricityBatch.class)
                    .keyName("inbound.UrbanInstitute.Discovergy.Batch.Elec")
                    .addSparkOption("timestampFormat", "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZ")
                    .build();
    public static final Key<GasBatch> INBOUND_URBANINSTITUTE_DISCOVERGY_GAS_BATCH =
            factory.getBuilder(GasBatch.class)
                    .keyName("inbound.UrbanInstitute.Discovergy.Batch.Gas")
                    .addSparkOption("timestampFormat", "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZ")
                    .build();
    public static final Key<WaterBatch> INBOUND_URBANINSTITUTE_DISCOVERGY_WATER_BATCH =
            factory.getBuilder(WaterBatch.class)
                    .keyName("inbound.UrbanInstitute.Discovergy.Batch.Water")
                    .addSparkOption("timestampFormat", "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZ")
                    .build();

    public static final Key<OpenWeatherMapEvent> INBOUND_URBANINSTITUTE_OPENWEATHER =
            factory.getBuilder(OpenWeatherMapEvent.class)
                    .keyName("inbound.UrbanInstitute.OpenWeather")
                    .addSparkOption("timestampFormat", "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZ")
                    .build();

    public static final Key<TestApi> INBOUND_TESTAPI_KEY =
            factory.getBuilder(TestApi.class)
                    .keyName("inbound.TestApi.Key")
                    .addSparkOption("timestampFormat", "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZ")
                    .build();

    public static final Key<DiscovergyReading> INBOUND_DISCOVERGY_METER_ALL =
            factory.getBuilder(DiscovergyReading.class)
                    .keyName("inbound.Discovergy.Meter.ALL")
                    .addSparkOption("timestampFormat", "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZ")
                    .build();

    public static final Key<DiscovergyReadingBatch> INBOUND_DISCOVERGY_BATCH =
            factory.getBuilder(DiscovergyReadingBatch.class)
                    .keyName("inbound.Discovergy.Batch")
                    .orderPartitionByKey(true)
                    .addSparkOption("timestampFormat", "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZ")
                    .build();

    public static final Key<DwdRecord> INBOUND_DWD =
            factory.getBuilder(DwdRecord.class)
                    .keyName("inbound.Discovergy.Dwd")
                    .addSparkOption("timestampFormat", "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZ")
                    .build();



    // Open Data
    public static final Key<CityWeather> OPENDATA_CITYWEATHER =
            factory.getBuilder(CityWeather.class)
                    .keyName("OpenData.CityWeather")
                    .build();
    public static final Key<Environment> OPENDATA_SMARTCITY_ENVIRONMENT =
            factory.getBuilder(Environment.class)
                    .keyName("OpenData.Environment")
                    .build();

    public static final Key<Mobility> OPENDATA_SMARTCITY_MOBILITY =
            factory.getBuilder(Mobility.class)
                    .keyName("OpenData.Mobility")
                    .build();

    public static final Key<Gas> OPENDATA_SMARTENERGY_GAS =
            factory.getBuilder(Gas.class)
                    .keyName("OpenData.SmartEnergy.Gas")
                    .addSparkOption("timestampFormat", "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZ")
                    .build();

    public static final Key<Electricity> OPENDATA_SMARTENERGY_ELECTRICITY =
            factory.getBuilder(Electricity.class)
                    .keyName("OpenData.SmartEnergy.Electricity")
                    .addSparkOption("timestampFormat", "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZ")
                    .build();

    public static final Key<Water> OPENDATA_SMARTENERGY_WATER =
            factory.getBuilder(Water.class)
                    .keyName("OpenData.SmartEnergy.Water")
                    .addSparkOption("timestampFormat", "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZ")
                    .build();

    public static final Key<Departure> OPENDATA_RMV_DEPARTURE =
            factory.getBuilder(Departure.class)
                    .keyName("OpenData.RMV.Departure")
                    .addSparkOption("timestampFormat", "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZ")
                    .build();

    public static final Key<DwdForecastRecord> OPENDATA_DWD_FORECAST =
            factory.getBuilder(DwdForecastRecord.class)
                    .keyName("OpenData.DWD.DwdRecord")
                    .addSparkOption("timestampFormat", "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZ")
                    .build();


    public static Key<?> valueOf(String keyName) {
        return factory.keys.get(keyName);
    }
}