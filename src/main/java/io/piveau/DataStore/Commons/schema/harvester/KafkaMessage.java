package io.piveau.DataStore.Commons.schema.harvester;

import java.io.Serializable;
import java.sql.Timestamp;

public abstract class KafkaMessage implements Serializable {

    private Timestamp timestamp;

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }
}
