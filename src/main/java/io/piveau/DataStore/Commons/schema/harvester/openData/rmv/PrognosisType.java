package io.piveau.DataStore.Commons.schema.harvester.openData.rmv;

// PrognosisType provides the type of the prognosis like if the prognosis was reported by an external provider or
// calculated or corrected by the system.
public enum PrognosisType {
    PROGNOSED, MANUAL, REPORTED, CORRECTED, CALCULATED
}
