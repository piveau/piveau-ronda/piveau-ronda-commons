package io.piveau.DataStore.Commons.schema.harvester.openData.dwd;

public class Forecast {

    private String Uhrzeit;
    private String Luft;
    private String Niederschlag;
    private String Bewoelkung;
    private String Wind;
    private Streckentyp Innenstadt;
    private Streckentyp Bruecke;
    private Streckentyp Nebenstrecke;

    public String getUhrzeit() {
        return Uhrzeit;
    }

    public void setUhrzeit(String uhrzeit) {
        Uhrzeit = uhrzeit;
    }

    public String getLuft() {
        return Luft;
    }

    public void setLuft(String luft) {
        Luft = luft;
    }

    public String getNiederschlag() {
        return Niederschlag;
    }

    public void setNiederschlag(String niederschlag) {
        Niederschlag = niederschlag;
    }

    public String getBewoelkung() {
        return Bewoelkung;
    }

    public void setBewoelkung(String bewoelkung) {
        Bewoelkung = bewoelkung;
    }

    public String getWind() {
        return Wind;
    }

    public void setWind(String wind) {
        Wind = wind;
    }

    public Streckentyp getInnenstadt() {
        return Innenstadt;
    }

    public void setInnenstadt(Streckentyp innenstadt) {
        Innenstadt = innenstadt;
    }

    public Streckentyp getBruecke() {
        return Bruecke;
    }

    public void setBruecke(Streckentyp bruecke) {
        Bruecke = bruecke;
    }

    public Streckentyp getNebenstrecke() {
        return Nebenstrecke;
    }

    public void setNebenstrecke(Streckentyp nebenstrecke) {
        Nebenstrecke = nebenstrecke;
    }
}
