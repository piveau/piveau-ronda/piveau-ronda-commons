package io.piveau.DataStore.Commons.schema.harvester.inbound.urbanInstitute;

import io.piveau.DataStore.Commons.schema.harvester.KafkaMessage;

public class WifiAccessPointInsertEvent extends KafkaMessage {

    private String layerName;
    private Integer connectedClients;
    private Integer totalClients;

    public String getLayerName() {
        return layerName;
    }

    public void setLayerName(String layerName) {
        this.layerName = layerName;
    }

    public Integer getConnectedClients() {
        return connectedClients;
    }

    public void setConnectedClients(Integer connectedClients) {
        this.connectedClients = connectedClients;
    }

    public Integer getTotalClients() {
        return totalClients;
    }

    public void setTotalClients(Integer totalClients) {
        this.totalClients = totalClients;
    }
}
