package io.piveau.DataStore.Commons.schema;

public enum DataFormat {
    CSV("csv"),
    JSON("json");

    public final String fileExtension;

    DataFormat(String fileExtension) {
        this.fileExtension = fileExtension;
    }

    public static DataFormat byFileExtension(String fileExtension) {
        String lowerFormat = fileExtension.toLowerCase();
        for (DataFormat format : values()) {
            if (format.fileExtension.equals(lowerFormat))
                return format;
        }
        return null;
    }

    public static DataFormat byMimeType(String mimeType) {
        if (mimeType == null)
            return null;

        switch (mimeType) {
            case "application/json":
                return DataFormat.JSON;
            case "text/csv":
                return DataFormat.CSV;
            default:
                return null;
        }
    }
}
