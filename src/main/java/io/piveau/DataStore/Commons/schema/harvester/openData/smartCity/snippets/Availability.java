package io.piveau.DataStore.Commons.schema.harvester.openData.smartCity.snippets;

public class Availability {
    public Float available;
    public Float inUse;
    public Float total;

    public Float getAvailable() {
        return available;
    }

    public void setAvailable(Float available) {
        this.available = available;
    }

    public Float getInUse() {
        return inUse;
    }

    public void setInUse(Float inUse) {
        this.inUse = inUse;
    }

    public Float getTotal() {
        return total;
    }

    public void setTotal(Float total) {
        this.total = total;
    }

}
