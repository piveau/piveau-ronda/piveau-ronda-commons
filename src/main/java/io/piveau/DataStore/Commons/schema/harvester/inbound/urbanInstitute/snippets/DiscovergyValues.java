package io.piveau.DataStore.Commons.schema.harvester.inbound.urbanInstitute.snippets;

public class DiscovergyValues {
    Long energyOut;
    Long energy2;
    Long energy1;
    Long energyOut1;
    Long power;
    Long energyOut2;
    Long energy;
    Long volume;
    Long actualityDuration;
    Long volumeFlow;
    Long power1;
    Long power2;
    Long power3;

    public Long getPower1() {
        return power1;
    }

    public void setPower1(Long power1) {
        this.power1 = power1;
    }

    public Long getPower2() {
        return power2;
    }

    public void setPower2(Long power2) {
        this.power2 = power2;
    }

    public Long getPower3() {
        return power3;
    }

    public void setPower3(Long power3) {
        this.power3 = power3;
    }

    public Long getVolumeFlow() {
        return volumeFlow;
    }

    public void setVolumeFlow(Long volumeFlow) {
        this.volumeFlow = volumeFlow;
    }


    public Long getEnergyOut() {
        return energyOut;
    }

    public void setEnergyOut(Long energyOut) {
        this.energyOut = energyOut;
    }

    public Long getEnergy2() {
        return energy2;
    }

    public void setEnergy2(Long energy2) {
        this.energy2 = energy2;
    }

    public Long getEnergy1() {
        return energy1;
    }

    public void setEnergy1(Long energy1) {
        this.energy1 = energy1;
    }

    public Long getEnergyOut1() {
        return energyOut1;
    }

    public void setEnergyOut1(Long energyOut1) {
        this.energyOut1 = energyOut1;
    }

    public Long getPower() {
        return power;
    }

    public void setPower(Long power) {
        this.power = power;
    }

    public Long getEnergyOut2() {
        return energyOut2;
    }

    public void setEnergyOut2(Long energyOut2) {
        this.energyOut2 = energyOut2;
    }

    public Long getEnergy() {
        return energy;
    }

    public void setEnergy(Long energy) {
        this.energy = energy;
    }

    public Long getVolume() {
        return volume;
    }

    public void setVolume(Long volume) {
        this.volume = volume;
    }

    public Long getActualityDuration() {
        return actualityDuration;
    }

    public void setActualityDuration(Long actualityDuration) {
        this.actualityDuration = actualityDuration;
    }


}
