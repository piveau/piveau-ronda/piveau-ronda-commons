package io.piveau.DataStore.Commons.schema.harvester.openData.rmv;

import io.piveau.DataStore.Commons.schema.harvester.KafkaMessage;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;

// The element Departure contains all information about a departure like time, date, stop/station name, track, realtime
// time, date and track, direction, name and type of the journey. It also contains a reference to journey details.
public class Departure extends KafkaMessage {

    private String journeyDetailRef;

    private String name;                    // name of departing journey as used for display
    private String type;                    // type of departing journey
    private String stop;                    // name of the stop/station
    private String stopId;                  // ID of the stop
    private String stopExtId;               // external ID of stop
    private PrognosisType prognosisType;    // prognosis type of of departure date and time
    private Boolean cancelled;              // true if journey has been cancelled
    private Boolean partCancelled;          // true if journey has been partially cancelled
    private Boolean reachable;              // true if journey is reachable
    private Boolean redirected;             // true if journey has been redirected
    private Boolean isFastest;              // services is 'fastest service to' location
    private String trainNumber;             // train number as used for display
    private String trainCategory;           // train category as used for display

    private String direction;       // direction, last stop of the journey
    private String track;           // track information
    private String rtTrack;         // realtime track information

    private String time;         // time
    private String date;         // date
    private String timeZone;        // time zone
    private String rtTime;       // realtime time
    private String rtDate;       // realtime date
    private String rtTimeZone;      // realtime time zone

    private String timeAtArrival;        // time the services arrive at the destination
    private String dateAtArrival;        // date the services arrive at the destination
    private String rtTimeAtArrival;      // realtime time the services arrive at the destination
    private String rtDateAtArrival;      // realtime date the services arrive at the destination


    public String getJourneyDetailRef() {
        return journeyDetailRef;
    }

    public void setJourneyDetailRef(String journeyDetailRef) {
        this.journeyDetailRef = journeyDetailRef;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStop() {
        return stop;
    }

    public void setStop(String stop) {
        this.stop = stop;
    }

    public String getStopId() {
        return stopId;
    }

    public void setStopId(String stopId) {
        this.stopId = stopId;
    }

    public String getStopExtId() {
        return stopExtId;
    }

    public void setStopExtId(String stopExtId) {
        this.stopExtId = stopExtId;
    }

    public PrognosisType getPrognosisType() {
        return prognosisType;
    }

    public void setPrognosisType(PrognosisType prognosisType) {
        this.prognosisType = prognosisType;
    }

    public Boolean getCancelled() {
        return cancelled;
    }

    public void setCancelled(Boolean cancelled) {
        this.cancelled = cancelled;
    }

    public Boolean getPartCancelled() {
        return partCancelled;
    }

    public void setPartCancelled(Boolean partCancelled) {
        this.partCancelled = partCancelled;
    }

    public Boolean getReachable() {
        return reachable;
    }

    public void setReachable(Boolean reachable) {
        this.reachable = reachable;
    }

    public Boolean getRedirected() {
        return redirected;
    }

    public void setRedirected(Boolean redirected) {
        this.redirected = redirected;
    }

    public Boolean getFastest() {
        return isFastest;
    }

    public void setFastest(Boolean fastest) {
        isFastest = fastest;
    }

    public String getTrainNumber() {
        return trainNumber;
    }

    public void setTrainNumber(String trainNumber) {
        this.trainNumber = trainNumber;
    }

    public String getTrainCategory() {
        return trainCategory;
    }

    public void setTrainCategory(String trainCategory) {
        this.trainCategory = trainCategory;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getTrack() {
        return track;
    }

    public void setTrack(String track) {
        this.track = track;
    }

    public String getRtTrack() {
        return rtTrack;
    }

    public void setRtTrack(String rtTrack) {
        this.rtTrack = rtTrack;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public String getRtTime() {
        return rtTime;
    }

    public void setRtTime(String rtTime) {
        this.rtTime = rtTime;
    }

    public String getRtDate() {
        return rtDate;
    }

    public void setRtDate(String rtDate) {
        this.rtDate = rtDate;
    }

    public String getRtTimeZone() {
        return rtTimeZone;
    }

    public void setRtTimeZone(String rtTimeZone) {
        this.rtTimeZone = rtTimeZone;
    }

    public String getTimeAtArrival() {
        return timeAtArrival;
    }

    public void setTimeAtArrival(String timeAtArrival) {
        this.timeAtArrival = timeAtArrival;
    }

    public String getDateAtArrival() {
        return dateAtArrival;
    }

    public void setDateAtArrival(String dateAtArrival) {
        this.dateAtArrival = dateAtArrival;
    }

    public String getRtTimeAtArrival() {
        return rtTimeAtArrival;
    }

    public void setRtTimeAtArrival(String rtTimeAtArrival) {
        this.rtTimeAtArrival = rtTimeAtArrival;
    }

    public String getRtDateAtArrival() {
        return rtDateAtArrival;
    }

    public void setRtDateAtArrival(String rtDateAtArrival) {
        this.rtDateAtArrival = rtDateAtArrival;
    }
}
