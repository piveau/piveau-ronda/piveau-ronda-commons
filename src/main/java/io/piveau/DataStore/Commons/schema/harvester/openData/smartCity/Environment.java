package io.piveau.DataStore.Commons.schema.harvester.openData.smartCity;

import io.piveau.DataStore.Commons.schema.harvester.KafkaMessage;
import io.piveau.DataStore.Commons.schema.harvester.openData.smartCity.snippets.Availability;
import io.piveau.DataStore.Commons.schema.harvester.openData.snippets.Coordinates;
import io.piveau.DataStore.Commons.schema.harvester.openData.snippets.TypedValue;

public class Environment extends KafkaMessage {

    private String type;
    private TypedValue<String> value;
    private Availability availability;
    private Coordinates coordinates;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Availability getAvailability() {
        return availability;
    }

    public void setAvailability(Availability availability) {
        this.availability = availability;
    }

    public TypedValue<String> getValue() {
        return value;
    }

    public void setValue(TypedValue<String> value) {
        this.value = value;
    }

    public Coordinates getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(Coordinates coordinates) {
        this.coordinates = coordinates;
    }
}
