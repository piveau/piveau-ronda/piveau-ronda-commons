package io.piveau.DataStore.Commons.schema.harvester.inbound.urbanInstitute;

import io.piveau.DataStore.Commons.schema.harvester.KafkaMessage;

public class WaveScapeEvent extends KafkaMessage {

    private Float latitude;
    private Float longitude;
    private Float location;
    private Float measurement;

    public Float getLongitude() {
        return longitude;
    }

    public void setLongitude(Float longitude) {
        this.longitude = longitude;
    }

    public Float getLocation() {
        return location;
    }

    public void setLocation(Float location) {
        this.location = location;
    }

    public Float getMeasurement() {
        return measurement;
    }

    public void setMeasurement(Float measurement) {
        this.measurement = measurement;
    }

    public Float getLatitude() {
        return latitude;
    }

    public void setLatitude(Float latitude) {
        this.latitude = latitude;
    }
}
