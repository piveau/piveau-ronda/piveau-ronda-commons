package io.piveau.DataStore.Commons.schema.harvester.inbound.openWeatherMap;

import io.piveau.DataStore.Commons.schema.harvester.KafkaMessage;
import io.piveau.DataStore.Commons.schema.harvester.inbound.openWeatherMap.snippets.Coordinates;
import io.piveau.DataStore.Commons.schema.harvester.inbound.openWeatherMap.snippets.Main;

public class OpenWeatherMap extends KafkaMessage {

    private Coordinates coord;
    private Main main;

    public Coordinates getCoord() {
        return coord;
    }

    public void setCoord(Coordinates coord) {
        this.coord = coord;
    }

    public Main getMain() {
        return main;
    }

    public void setMain(Main main) {
        this.main = main;
    }
}
