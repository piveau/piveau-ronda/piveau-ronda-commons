package io.piveau.DataStore.Commons.schema.harvester.inbound.dwd;

import io.piveau.DataStore.Commons.schema.harvester.KafkaMessage;

public class DwdRecord extends KafkaMessage {

    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
