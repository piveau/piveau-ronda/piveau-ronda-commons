package io.piveau.DataStore.Commons.schema.harvester.openData.smartEnergy.water;

import io.piveau.DataStore.Commons.schema.harvester.KafkaMessage;
import io.piveau.DataStore.Commons.schema.harvester.openData.smartEnergy.snippets.Statistics;

public class Water extends KafkaMessage {

    private Statistics volumeFlow;

    public Statistics getVolumeFlow() {
        return volumeFlow;
    }

    public void setVolumeFlow(Statistics volumeFlow) {
        this.volumeFlow = volumeFlow;
    }
}
